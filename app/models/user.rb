class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

	validates_presence_of :phone
	validates_presence_of :name

  has_many :events, dependent: :destroy
  accepts_nested_attributes_for :events,  allow_destroy: true

end
