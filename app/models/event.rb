class Event < ApplicationRecord

	validates_presence_of :event_date, :event_time, :people

	belongs_to :user

	def cancelar!
		self.update(canceled: true)
	end

end
