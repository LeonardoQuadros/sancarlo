module EventHelper
	def meus_eventos_ativos user = current_user
		if user.blank?
			return nil
		else
			user.events.where("events.event_date > '#{Date.yesterday}'")
		end
	end

	def event_status(event)
		if event.canceled
			return "<span class='span red-text'>Cancelado</span>".html_safe
		elsif event.confirmed
			return "<span class='span green-text'>Confirmado</span>".html_safe
		else
			return "<span class='span orange-text'>Aguardando...</span>".html_safe
		end
	end

end
