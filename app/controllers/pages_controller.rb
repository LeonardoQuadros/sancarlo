class PagesController < ApplicationController
  def home
		@images = Dir.glob("public/images/banners/*.jpg")
		@examples = Dir.glob("public/images/examples/*.jpg")

  end

  def reserva
  	current_user.blank? ? @user = User.new : @event = Event.new(user_id: current_user.id)

  end

end
