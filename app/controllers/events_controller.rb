class EventsController < ApplicationController

	def create
    @event = Event.new(event_params)
    @event.user_id = current_user.id


    begin
      a = params[:event][:event_date].to_date
    rescue Exception => @erro
    end


    @event.save
    @event.errors.messages[:erro] = ["#{@erro}"] unless @erro.blank?
    respond_to do |format|
      format.js  
    end

  end

  def update
    begin
      @event = Event.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      respond_to do |format|
        format.js {render inline: "M.toast({html: '#{e.message}', classes: 'red'}); " }
      end
    end
    @event.update(event_params)
    respond_to do |format|
      format.js  
    end
  end

  def cancelar
    begin
      @event = Event.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      respond_to do |format|
        format.js {render inline: "M.toast({html: '#{e.message}', classes: 'red'}); " }
      end
    end
    if @event.cancelar!
      redirect_to request.referrer, alert: "Evento cancelado"
    else
      respond_to do |format|
        format.js {render inline: "M.toast({html: '#{@event.errors.full_messages.first}', classes: 'red'}); " }
      end
    end

  end

  def nova_reserva
    @event = params[:id].blank? ? Event.new : Event.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  private

  def event_params
    params.require(:event).permit(:event_date, :event_time, :people, :message, :user_id)
  end

end
