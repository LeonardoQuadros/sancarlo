class UsersController < ApplicationController

	def novo_contato
    u = User.new(user_params)
    u.user_agent = request.env['HTTP_USER_AGENT']
    u.remote_ip = request.remote_ip
    u.password = Devise.friendly_token
    l = Location.geocode(Rails.env.development? ? '179.154.141.205' : u.remote_ip)
    u.location = l.to_json unless l.blank?


    @user = User.find_or_create_by(email: u.email) do |user|
      user.name = u.name
      user.password = u.password
      user.phone = u.phone
      user.user_agent = u.user_agent
      user.remote_ip = u.remote_ip
      user.location = u.location
    end

    if @user.valid?
      sign_in(:user, @user)
      redirect_to request.referrer, notice: "Salvo!"
    else
      respond_to do |format|
        format.js  
      end
    end





  #@user.errors.messages[:erro] = ["#{@erro}"] unless @erro.blank?
  end

  private

  def user_params
    params.require(:user).permit(:email, :name, :phone, 
                                  events_attributes: [:id, :event_date, :event_time, :people])
  end
end
