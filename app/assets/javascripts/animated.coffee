$(document).on 'turbolinks:load', ->
  $(window).scroll ->
    $('.show-off').each (i) ->
      bottom_of_object = $(this).offset().top 
      bottom_of_window = $(window).scrollTop() + $(window).height()
      if bottom_of_window > bottom_of_object and $(window).width() > 992
        $(this).addClass "animated fadeIn slow"
        $(this).css "opacity", 1
      return
    return
  if $(window).width() <= 992
    $('.show-off').css "opacity", 1
    return
  return