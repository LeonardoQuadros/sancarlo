$(document).on 'turbolinks:load', ->
  $('[data-toggle="datepicker"]').datepicker language: 'pt-BR'
  $('.sidenav').sidenav()
  $('.slider').slider()
  $('.datepicker').datepicker()
  $('.collapsible').collapsible()
  $('.parallax').parallax()
  $('[data-toggle="tooltip"]').tooltip()
  $('select').formSelect()
  $('.parallax-container').css 'height', 450
  $('.video-parallax-container').height $(window).height()
  M.updateTextFields();
  $('.carousel.carousel-slider').carousel
    fullWidth: true
    indicators: true
  return



$(document).on 'turbolinks:before-cache', ->
  elem = document.querySelector('#slide-out')
  if elem
    instance = M.Sidenav.getInstance(elem)
  if instance
    instance.destroy()
  return
  