class AddCamposComplementaresToUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :name, :string
  	add_column :users, :phone, :string
  	add_column :users, :admin, :boolean, default: false
  	add_column :users, :remote_ip, :string
  	add_column :users, :user_agent, :string
  	add_column :users, :location, :json, default: {}
  	add_column :users, :cpf, :string
  	add_column :users, :rg, :string
  	add_column :users, :date_birth, :string
  	add_column :users, :confirmed, :boolean, default: false
  end
end
