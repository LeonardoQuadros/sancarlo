class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
    	t.string 													:name
    	t.text														:message
    	t.date 													:event_date
    	t.string 													:event_time
    	t.integer 												:people

    	t.references											:type, index: true
    	t.references											:service, index: true
    	t.references											:user, index: true
    	
    	t.boolean 												:reserved, default: false
        t.boolean                                               :confirmed, default: false
    	t.boolean 												:canceled, default: false
    	t.datetime 												:confirmed_at

      t.timestamps
    end
  end
end
