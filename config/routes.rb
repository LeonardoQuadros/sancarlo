Rails.application.routes.draw do



  ########################################################################################################
  
  namespace :dashboard do
    
    controller :pages do
      get '/home' => :home
    end
  	
  	controller :users do
  	end
    resources :users

    root 'pages#home'
    get '*path' => redirect('/')
  end

  ########################################################################################################

  devise_for :users

  controller :pages do
    get '/home' => :home
    get '/reserva' => :reserva
  end

  controller :users do
    post "users/novo_contato" => :novo_contato
  end
  
  controller :events do
    get "events/nova_reserva" => :nova_reserva
    post "events/cancelar/:id" => :cancelar, as: :events_cancelar
  end
  resources :events
    

  root "pages#home"
  get '*path' => redirect('/')
end
